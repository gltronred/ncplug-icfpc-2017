{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE DeriveGeneric #-}

module Strategies.MST where

import           Strategies.Common
import           Types

import           Data.Aeson
import           Data.Function
import           Data.Graph.Inductive.Graph
import           Data.Graph.Inductive.PatriciaTree
import           Data.Graph.Inductive.Query.MST
import           Data.List
import           Data.Ord
import           Data.Set (Set)
import qualified Data.Set as S
import           Data.Text (Text)
import           GHC.Generics

type MST = (Node, LRTree Int)
data State = State { curGraph :: !ActualMap
                   , curTrees :: ![MST]
                   , curEdges :: !(Set Edge)
                   , curNodes :: !(Set Node)
                   , curMines :: !(Set Node)
                   , curRot :: !Int
                   , curMe :: !PunterId
                   } deriving (Eq,Show,Generic)
instance FromJSON State where
  parseJSON = genericParseJSON $ removePrefix 3
instance ToJSON State where
  toJSON = genericToJSON $ removePrefix 3
  toEncoding = genericToEncoding $ removePrefix 3

mstStrategy :: Strategy State
mstStrategy = Strategy
  { dispatchSetup = setup
  , dispatchGameStep = \a b -> pure $ gameStep a b
  , makeMove = pure . move
  , prettyPrintState = pp
  }

setup :: SetupReq -> (Maybe [Futures], State)
setup SetupReq { sqMap = gameMap, sqPunter = me } = let
  edgeGraph = createActualMap gameMap
  mines = map unSiteId $ gmMines gameMap
  mineTrees = createMineTrees edgeGraph mines
  in (Nothing, State edgeGraph mineTrees S.empty S.empty (S.fromList mines) 0 me)

createMineTrees :: ActualMap -> [Int] -> [MST]
createMineTrees edgeGraph mines = let
  mineTrees = zip mines $ map (flip msTreeAt edgeGraph) mines
  mineTrees' = sortTrees mineTrees
  mineTrees'' = map (\(r,t) -> (r, map (LP . reverse . unLPath) t)) mineTrees'
  in mineTrees''

sortTrees :: [MST] -> [MST]
sortTrees = map (\(r,t) -> (r, sortPaths t)) . sortBy (comparing $ Down . longestTreePath . snd)

longestTreePath :: LRTree Int -> Int
longestTreePath tree = maximum $ 0 : map (length . unLPath) tree

sortPaths :: LRTree Int -> LRTree Int
sortPaths = sortBy (comparing $ Down . length . unLPath)

gameStep :: GameReq -> State -> State
gameStep (GameReq moves) (State curGraph curTrees myEdges myNodes mines rot me) = let
  newGraph = removeMovedEdges me moves curGraph
  newTrees = rotate rot $ createMineTrees newGraph $ S.toList mines
  newTrees' = map (removeClaimedNodes myNodes) newTrees
  newTrees'' = dropWhile (null . snd) newTrees'
  -- newTrees = createMineTrees newGraph mines
  in case newTrees'' of
       [] -> State newGraph newTrees'' myEdges myNodes mines (nextRot newTrees'' rot) me
       trees@(_:_) -> let
         trees' = map (removeClaimedEdges myEdges) trees
         newTrees''' = dropWhile null trees'
         in State newGraph newTrees''' myEdges myNodes mines (nextRot newTrees''' rot) me

nextRot :: [a] -> Int -> Int
nextRot as rot = if null as
                 then 0
                 else (rot+1) `mod` length as

rotate :: Int -> [a] -> [a]
rotate k as = let
  before = take k as
  after = drop k as
  in after ++ before

removeClaimedNodes :: Set Node -> MST -> MST
removeClaimedNodes myNodes (root, tree) = let
  tree' = filter (\(LP path) -> fst (last path) `S.notMember` myNodes) tree
  in (root, tree')

removeClaimedEdges :: Set Edge -> MST -> MST
removeClaimedEdges myEdges (root, tree) = let
  tree' = map (LP . removeClaimedEdge myEdges . unLPath) tree
  in (root, tree')

removeClaimedEdge :: Set Edge -> [LNode Int] -> [LNode Int]
removeClaimedEdge myEdges path = case path of
  [] -> []
  [x] -> [x]
  list@(xs@(x,_):ys@(y,_):zs) -> if (x,y) `S.member` myEdges
                                 then removeClaimedEdge myEdges $ ys:zs
                                 else list

move :: State -> (PunterId -> Move, State)
move state@(State graph trees edges nodes mines rot me) = case trees of
  [] -> claimAnyEdge state
  ((root,tree) : treeTail) -> case tree of
    [] -> claimAnyEdge state
    (path : paths) -> case unLPath path of
      [] -> claimAnyEdge state
      [x] -> claimAnyEdge state
      ((x,_):ys@(y,_):zs) -> let
        (cmd, nodes') = claimAndMark x y nodes
        edges' = S.insert (x,y) edges
        path' = LP $ ys:zs
        tree' = path' : paths
        trees' = (root, tree') : treeTail
        in (cmd, State graph trees' edges' nodes' mines rot me)

claimAnyEdge :: State -> (MoveFn, State)
claimAnyEdge state = case unclaimedEdge (curEdges state) (curGraph state) of
  Nothing -> (pass, state)
  Just (s,t) -> let
    (cmd, nodes') = claimAndMark s t (curNodes state)
    edges' = S.insert (s,t) (curEdges state)
    in (cmd, state { curNodes = nodes', curEdges = edges' })

unclaimedEdge :: Set Edge -> ActualMap -> Maybe Edge
unclaimedEdge claimedEdges graph = let
  allEdges = edges graph
  unclaimed = filter (`S.notMember` claimedEdges) allEdges
  in case unclaimed of
       [] -> Nothing
       (e:_) -> Just e

pp :: Maybe (State -> Text)
pp = Nothing

