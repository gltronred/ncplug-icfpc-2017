{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StandaloneDeriving #-}

module Main where

import           Lib
import           Strategies.Valuer
import           Types

import           Control.Monad.Par.Class
import           Criterion
import           Criterion.Main
import           Data.Aeson
import qualified Data.ByteString.Lazy as BS
import           Data.Maybe
import           Data.Set (Set)
import qualified Data.Set as S
import           GHC.Generics
import           System.Random

main :: IO ()
main = defaultMain
  [ bgroup "shortest path"
    [ env (setupEnv "randomSparse.json") $ \ ~(g, m, s, t) -> bench "randomSparse (86, 123)" $ whnf (sp s t) g
    , env (setupEnv "tube.json") $ \ ~(g, m, s, t) -> bench "tube (301, 386)" $ whnf (sp s t) g
    , env (setupEnv "boston-sparse.json") $ \ ~(g, m, s, t) -> bench "boston-sparse (488, 945)" $ whnf (sp s t) g
    , env (setupEnv "edinburgh-sparse.json") $ \ ~(g, m, s, t) -> bench "edinburgh-sparse (961, 1751)" $ whnf (sp s t) g
    ]
  , bgroup "loading"
    [ env (setupFile "randomSparse.json") $ \ ~f -> bench "randomSparse (86, 123)" $ whnf decodeX f
    , env (setupFile "tube.json") $ \ ~f -> bench "tube (301, 386)" $ whnf decodeX f
    , env (setupFile "boston-sparse.json") $ \ ~f -> bench "boston-sparse (488, 945)" $ whnf decodeX f
    , env (setupFile "edinburgh-sparse.json") $ \ ~f -> bench "edinburgh-sparse (961, 1751)" $ whnf decodeX f
    ]
  , bgroup "saving"
    [ env (setupState "randomSparse.json") $ \ ~f -> bench "randomSparse (86, 123)" $ whnf encode f
    , env (setupState "tube.json") $ \ ~f -> bench "tube (301, 386)" $ whnf encode f
    , env (setupState "boston-sparse.json") $ \ ~f -> bench "boston-sparse (488, 945)" $ whnf encode f
    , env (setupState "edinburgh-sparse.json") $ \ ~f -> bench "edinburgh-sparse (961, 1751)" $ whnf encode f
    ]
  , bgroup "computeDistances"
    [ env (setupEnv "randomSparse.json") $ \ ~(g, m, s, t) -> bench "randomSparse (86, 123)" $ whnf (computeDistances g) m
    , env (setupEnv "tube.json") $ \ ~(g, m, s, t) -> bench "tube (301, 386)" $ whnf (computeDistances g) m
    , env (setupEnv "boston-sparse.json") $ \ ~(g, m, s, t) -> bench "boston-sparse (488, 945)" $ whnf (computeDistances g) m
    , env (setupEnv "edinburgh-sparse.json") $ \ ~(g, m, s, t) -> bench "edinburgh-sparse (961, 1751)" $ whnf (computeDistances g) m
    ]
  , bgroup "mostPromising"
    [ env (setupState "randomSparse.json") $ \ ~f -> bench "randomSparse (86, 123)" $ nf mostPromissing f
    , env (setupState "tube.json") $ \ ~f -> bench "tube (301, 386)" $ nf mostPromissing f
    , env (setupState "boston-sparse.json") $ \ ~f -> bench "boston-sparse (488, 945)" $ nf mostPromissing f
    , env (setupState "edinburgh-sparse.json") $ \ ~f -> bench "edinburgh-sparse (961, 1751)" $ nf mostPromissing f
    ]
  , bgroup "toSpans"
    [ env (vsSpans <$> setupState "randomSparse.json") $ \ ~f -> bench "randomSparse (86, 123)" $ nf toSpans f
    , env (vsSpans <$> setupState "tube.json") $ \ ~f -> bench "tube (301, 386)" $ nf toSpans f
    , env (vsSpans <$> setupState "boston-sparse.json") $ \ ~f -> bench "boston-sparse (488, 945)" $ nf toSpans f
    , env (vsSpans <$> setupState "edinburgh-sparse.json") $ \ ~f -> bench "edinburgh-sparse (961, 1751)" $ nf toSpans f
    ]
  , bgroup "continuePath"
    [ env ((\f -> (f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "randomSparse.json") $ \ ~f -> bench "randomSparse (86, 123)" $ nf (uncurry continuePath) f
    , env ((\f -> (f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "tube.json") $ \ ~f -> bench "tube (301, 386)" $ nf (uncurry continuePath) f
    , env ((\f -> (f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "boston-sparse.json") $ \ ~f -> bench "boston-sparse (488, 945)" $ nf (uncurry continuePath) f
    , env ((\f -> (f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "edinburgh-sparse.json") $ \ ~f -> bench "edinburgh-sparse (961, 1751)" $ nf (uncurry continuePath) f
    ]
  , bgroup "pathValid"
    [ env ((\f -> (vsMap f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "randomSparse.json") $ \ ~f -> bench "randomSparse (86, 123)" $ nf (uncurry pathValid) f
    , env ((\f -> (vsMap f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "tube.json") $ \ ~f -> bench "tube (301, 386)" $ nf (uncurry pathValid) f
    , env ((\f -> (vsMap f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "boston-sparse.json") $ \ ~f -> bench "boston-sparse (488, 945)" $ nf (uncurry pathValid) f
    , env ((\f -> (vsMap f, map fst $ unLPath $ snd $ fromJust $ mostPromissing f)) <$> setupState "edinburgh-sparse.json") $ \ ~f -> bench "edinburgh-sparse (961, 1751)" $ nf (uncurry pathValid) f
    ]
  , bgroup "move"
    [ env (setupState "randomSparse.json") $ \ ~f -> bench "randomSparse (86, 123)" $ whnf move f
    , env (setupState "tube.json") $ \ ~f -> bench "tube (301, 386)" $ whnf move f
    , env (setupState "boston-sparse.json") $ \ ~f -> bench "boston-sparse (488, 945)" $ whnf move f
    , env (setupState "edinburgh-sparse.json") $ \ ~f -> bench "edinburgh-sparse (961, 1751)" $ whnf move f
    ]
  ]

decodeX :: BS.ByteString -> Maybe GameMap
decodeX = decode



setupFile :: FilePath -> IO BS.ByteString
setupFile file = BS.readFile ("bench/" ++ file)

deriving instance NFData (SiteId)
deriving instance NFData (PunterId)
deriving instance Generic a => Generic (LPath a)
deriving instance (Generic a, NFData a) => NFData (LPath a)
deriving instance NFData Route
deriving instance Generic Int
deriving instance NFData Futures
deriving instance NFData Distances
deriving instance NFData ValuerState

setupState :: FilePath -> IO ValuerState
setupState file = do
  Right gameMap <- eitherDecode <$> setupFile file
  let (_,state) = setup $ SetupReq (PunterId 0) 1 gameMap Nothing
  return state

setupEnv :: FilePath -> IO (Gr () Int, Set Int, Int, Int)
setupEnv file = do
  state <- setupState file
  let g = vsMap state
      vs = nodes g
      n = noNodes g
      ms = vsMines state
      m = length ms
  si <- randomRIO (0, m-1)
  ti <- randomRIO (0, n-1)
  return (g, ms, S.toList ms !! si, vs !! ti)

