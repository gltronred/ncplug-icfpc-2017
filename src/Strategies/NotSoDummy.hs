{-# LANGUAGE OverloadedStrings #-}

module Strategies.NotSoDummy where

import           Types

import           Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as M
import           Data.IntSet (IntSet)
import qualified Data.IntSet as S
import           Data.Text (Text)

type MapGraph = IntMap IntSet
type Firefly = (MapGraph, [River])

notSoDummyStrategy :: Strategy Firefly
notSoDummyStrategy = Strategy
  { dispatchGameStep = \a b -> pure $ gameStep a b
  , dispatchSetup = setup
  , makeMove = pure . move
  , prettyPrintState = Nothing
  }

setup :: SetupReq -> (Maybe [Futures], Firefly)
setup (SetupReq { sqMap = GameMap { gmRivers = rs, gmMines = ms, gmSites = ss } }) = let
  emptyGraph = M.fromList $ zip (map (unSiteId . sId) ss) $ repeat S.empty
  graph = foldr (updateGraph S.insert) emptyGraph rs
  mines = S.fromList $ map unSiteId ms
  mineEdges = M.filterWithKey (\k _ -> k `S.member` mines) graph
  otherEdges = M.filterWithKey (\k _ -> k `S.notMember` mines) graph
  edges = map (\(k,v) -> River (SiteId k) (SiteId $ S.findMin v)) $
          M.assocs mineEdges ++ M.assocs otherEdges
  in (Nothing, (graph, edges))

updateGraph :: (Int -> IntSet -> IntSet) -> River -> MapGraph -> MapGraph
updateGraph f (River (SiteId source) (SiteId target)) =
  M.adjust (f source) target .
  M.adjust (f target) source

updateMove :: Move -> MapGraph -> MapGraph
updateMove move graph = case move of
  Pass _ -> graph
  Claim _ s t -> updateGraph S.delete (River s t) graph

gameStep :: GameReq -> Firefly -> Firefly
gameStep (GameReq { gqMove = Moves ms }) (graph, edges) = let
  graph' = foldr updateMove graph ms
  edges' = filter (\(River (SiteId x) (SiteId y)) -> y `S.member` (graph' M.! x)) edges
  in (graph', edges')

move :: Firefly -> (PunterId -> Move, Firefly)
move (graph, edges) = case edges of
  [] -> (pass, (graph, edges))
  (River (SiteId x) (SiteId y) : edges') -> (claim x y, (graph, edges'))

