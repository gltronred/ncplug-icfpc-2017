{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StrictData #-}

module Types.Protocol where

import           Data.Aeson
import           Data.Aeson.Types
import           Data.Binary (Binary)
import qualified Data.Binary as Bin
import qualified Data.ByteString.Base64 as Base64
import           Data.ByteString.Lazy.Char8 (ByteString)
import qualified Data.ByteString.Lazy.Char8 as BS
import qualified Data.HashMap.Lazy as HM
import           Data.Text (Text)
import qualified Data.Text.Encoding as T
import           GHC.Generics

newtype PunterId = PunterId { unPunterId :: Int } deriving (Eq,Show,Read,Generic,Binary,FromJSON,ToJSON)
newtype SiteId = SiteId { unSiteId :: Int } deriving (Eq,Ord,Show,Read,Generic,Binary,FromJSON,ToJSON)

newtype Site = Site { sId :: SiteId } deriving (Eq,Show,Read,Generic)
instance FromJSON Site where
  parseJSON = genericParseJSON $ removePrefix 1
instance ToJSON Site where
  toJSON = genericToJSON $ removePrefix 1
  toEncoding = genericToEncoding $ removePrefix 1

data River = River { rSource :: SiteId
                   , rTarget :: SiteId
                   } deriving (Eq,Show,Read,Generic)
instance FromJSON River where
  parseJSON = genericParseJSON $ removePrefix 1
instance ToJSON River where
  toJSON = genericToJSON $ removePrefix 1
  toEncoding = genericToEncoding $ removePrefix 1

data Move = Claim { mPunter :: PunterId
                  , mSource :: SiteId
                  , mTarget :: SiteId
                  }
          | Pass { mPunter :: PunterId }
          | Splurge { mPunter :: PunterId
                    , mRoute :: [SiteId]
                    }
          | Option { mPunter :: PunterId
                   , mSource :: SiteId
                   , mTarget :: SiteId
                   }
          deriving (Eq,Show,Read,Generic)
instance FromJSON Move where
  parseJSON = genericParseJSON $ removePrefix 1
instance ToJSON Move where
  toJSON = genericToJSON $ removePrefix 1
  toEncoding = genericToEncoding $ removePrefix 1

data GameMap = GameMap { gmSites :: [Site]
                       , gmRivers :: [River]
                       , gmMines :: [SiteId]
                       } deriving (Eq,Show,Read,Generic)
instance FromJSON GameMap where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON GameMap where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

data Score = Score { scorePunter :: PunterId
                   , scoreScore :: Int
                   } deriving (Eq,Show,Read,Generic)
instance FromJSON Score where
  parseJSON = genericParseJSON $ removePrefix 5
instance ToJSON Score where
  toJSON = genericToJSON $ removePrefix 5
  toEncoding = genericToEncoding $ removePrefix 5

newtype HandshakeReq = HandshakeReq { hqYou :: Text } deriving (Eq,Show,Read,Generic)
instance FromJSON HandshakeReq where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON HandshakeReq where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

newtype HandshakeResp = HandshakeResp { hqMe :: Text } deriving (Eq,Show,Read,Generic)
instance FromJSON HandshakeResp where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON HandshakeResp where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

data Settings = Settings { setFutures :: Maybe Bool
                         , setSplurges :: Maybe Bool
                         , setOptions :: Maybe Bool
                         } deriving (Eq,Show,Read,Generic)
instance FromJSON Settings where
  parseJSON = genericParseJSON $ removePrefix 3
instance ToJSON Settings where
  toJSON = genericToJSON $ removePrefix 3
  toEncoding = genericToEncoding $ removePrefix 3

data SetupReq = SetupReq { sqPunter :: PunterId
                         , sqPunters :: Int
                         , sqMap :: GameMap
                         , sqSettings :: Maybe Settings
                         } deriving (Eq,Show,Read,Generic)
instance FromJSON SetupReq where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON SetupReq where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

data SetupResp = SetupResp { spReady :: PunterId
                           , spFutures :: Maybe [Futures]
                           } deriving (Eq,Show,Read,Generic)
instance FromJSON SetupResp where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON SetupResp where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

newtype Moves = Moves { unMoves :: [Move] } deriving (Eq,Show,Read,Generic)
instance FromJSON Moves where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON Moves where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

data GameReq = GameReq { gqMove :: Moves
                       } deriving (Eq,Show,Read,Generic)
instance FromJSON GameReq where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON GameReq where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

newtype GameResp = GameResp { gpMove :: Move } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)

data StopScores = StopScores { ssMoves :: [Move]
                             , ssScores :: [Score]
                             } deriving (Eq,Show,Read,Generic)
instance FromJSON StopScores where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON StopScores where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

data StopReq = StopReq { stopStop :: StopScores
                       } deriving (Eq,Show,Read,Generic)
instance FromJSON StopReq where
  parseJSON = genericParseJSON $ removePrefix 4
instance ToJSON StopReq where
  toJSON = genericToJSON $ removePrefix 4
  toEncoding = genericToEncoding $ removePrefix 4

---------- Futures

data Futures = Futures { futSource :: SiteId
                       , futTarget :: SiteId
                       } deriving (Eq,Ord,Show,Read,Generic)
instance FromJSON Futures where
  parseJSON = genericParseJSON $ removePrefix 3
instance ToJSON Futures where
  toJSON = genericToJSON $ removePrefix 3
  toEncoding = genericToEncoding $ removePrefix 3

---------- Protocol

data Request = HandshakeRq HandshakeReq
             | SetupRq SetupReq
             | GameRq GameReq
             | StopRq StopReq
             deriving (Eq,Show,Read,Generic)
instance FromJSON Request where
  parseJSON = genericParseJSON untaggedObjects
instance ToJSON Request where
  toJSON = genericToJSON untaggedObjects
  toEncoding = genericToEncoding untaggedObjects

data Response = HandshakeRes HandshakeResp
              | SetupRes SetupResp
              | GameRes GameResp
              deriving (Eq,Show,Read,Generic)
instance FromJSON Response where
  parseJSON = genericParseJSON untaggedObjects
instance ToJSON Response where
  toJSON = genericToJSON untaggedObjects
  toEncoding = genericToEncoding untaggedObjects

type OnlineReq = Request
type OnlineResp = Response

encodeState :: Binary a => a -> Text
encodeState = T.decodeUtf8 . Base64.encode . BS.toStrict . Bin.encode

decodeState :: Binary a => Text -> a
decodeState = Bin.decode .
              either (error . ("Can't parse base64: " ++)) BS.fromStrict .
              Base64.decode .
              T.encodeUtf8

newtype Bin a = Bin { unBin :: a} deriving (Eq,Show,Read,Generic)
instance Binary a => FromJSON (Bin a) where
  parseJSON v = Bin . decodeState <$> parseJSON v
instance Binary a => ToJSON (Bin a) where
  toJSON (Bin a) = toJSON $ encodeState a
  toEncoding (Bin a) = toEncoding $ encodeState a

data OfflineReq game = OfflineReq { offRequest :: Request
                                  , offPrevState :: (Maybe game)
                                  } deriving (Eq,Show,Read)
instance FromJSON game => FromJSON (OfflineReq game) where
  parseJSON (Object v) = OfflineReq
                         <$> (parseJSON $ Object v)
                         <*> (parseState <$> v .:? "state")
  parseJSON invalid = typeMismatch "OfflineReq" invalid

parseState :: FromJSON a => Maybe Value -> Maybe a
parseState Nothing = Nothing
parseState (Just v) = parseMaybe parseJSON v

parseBin :: Binary a => Maybe Value -> Maybe a
parseBin Nothing = Nothing
parseBin (Just (String v)) = Just $ decodeState v
parseBin (Just _) = Nothing

data OfflineResp game = OfflineResp { offResponse :: Response
                                    , offNextState :: (Maybe game)
                                    } deriving (Eq,Show,Read)
instance ToJSON game => ToJSON (OfflineResp game) where
  toJSON (OfflineResp resp Nothing) = toJSON resp
  toJSON (OfflineResp resp (Just state)) = let
    Object r = toJSON resp
    Object s = object ["state" .= toJSON state]
    in Object $ HM.union r s

encodeProtoResponse :: ToJSON a => a -> ByteString
encodeProtoResponse x = let
  bs = encode x
  in BS.concat [ BS.pack $ show $ BS.length bs, ":", bs ]

decodeProtoRequest :: FromJSON a => ByteString -> a
decodeProtoRequest = (either error id) . eitherDecode . BS.tail . BS.dropWhile (/=':')

removePrefix :: Int -> Options
removePrefix k = defaultOptions
  { fieldLabelModifier = camelTo2 '-' . drop k
  , constructorTagModifier = camelTo2 '-'
  , omitNothingFields = True
  , sumEncoding = ObjectWithSingleField
  }

untaggedObjects :: Options
untaggedObjects = defaultOptions { sumEncoding = UntaggedValue }
