# ICFPC 2017 solution of NCPLUG team

>
> They sought it with thimbles, they sought it with care;
>
> They pursued it with forks and hope;
>
> They threatened its life with a railway-share;
>
> They charmed it with smiles and soap.
> 

# How to build this solution

Installation requires Internet access because stack downloads (a lot of) packages from https://stackage.org

~~~
stack install --local-bin-path=. --system-ghc
~~~

# How to use this solution

Use in offline mode:
~~~
./punter
~~~

Use in online mode:
~~~
./punter <address> <port>
~~~

# Authors

- Mansur Ziatdinov aka gltronred
- Alexander Tchitchigin aka Gabriel
- Alexey Pirogov aka astynax


# Helper scripts

## Review punter output as PNG

~~~
./punter <address> <port> | tee output.txt  # save punter output
cat output.txt | ./reviewer /tmp/review/    # create png from this output
feh -ZF /tmp/review/                        # use your favourite viewer to view them
~~~

