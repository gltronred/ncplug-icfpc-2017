{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TupleSections #-}

module Strategies.Valuer where

import           Strategies.Common
import           Types hiding (Heap)

import           Data.Aeson
import           Data.Aeson.Types
import           Data.Foldable
import           Data.Function
import           Data.Graph.Inductive.Internal.RootPath
import           Data.Heap (Heap, Entry (..))
import qualified Data.Heap as H
import           Data.List
import           Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as M
import           Data.IntSet (IntSet)
import qualified Data.IntSet as IS
import           Data.Maybe
import           Data.Monoid
import           Data.Ord
import           Data.Set (Set)
import qualified Data.Set as S
import           Debug.Trace
import           GHC.Generics

deriving instance Read a => Read (LPath a)

instance (FromJSON a, Ord a) => FromJSON (Heap a) where
  parseJSON v = H.fromList <$> parseJSON v
instance ToJSON a => ToJSON (Heap a) where
  toJSON = toJSON . H.toUnsortedList
  toEncoding = toEncoding . H.toUnsortedList

instance (FromJSON a, FromJSON b) => FromJSON (Entry a b) where
  parseJSON (Object v) = Entry <$> v .: "prio" <*> v .: "load"
  parseJSON invalid = typeMismatch "Entry" invalid
instance (ToJSON a, ToJSON b) => ToJSON (Entry a b) where
  toJSON (Entry a b) = object [ "prio" .= a, "load" .= b ]
  toEncoding (Entry a b) = pairs ( "prio" .= a <> "load" .= b )

newtype Distances = Distances { unDistances :: IntMap (IntMap (Maybe (Int, LPath Int))) } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)

newtype Spans3 = Spans3 { unSpans3 :: Heap (Entry Int (Int,Int,LPath Int)) } deriving (Eq,Show,Read,Generic,FromJSON,ToJSON)

data ValuerState = VS { vsMines :: !IntSet
                      , vsInitialDistances :: !Distances
                      , vsFutures :: !(Set Futures)
                      , vsPunters :: !Int
                      , vsMe :: !PunterId
                      , vsMap :: !ActualMap
                      -- , vsOurVertices :: !IntSet
                      , vsOurEdges :: !(Set Edge)
                      , vsSpans :: !Spans3
                      , vsCurPath :: !Path
                      } deriving (Eq,Show,Read,Generic)
instance FromJSON ValuerState where
  parseJSON = genericParseJSON $ removePrefix 2
instance ToJSON ValuerState where
  toJSON = genericToJSON $ removePrefix 2
  toEncoding = genericToEncoding $ removePrefix 2

valuerStrategy :: Strategy ValuerState
valuerStrategy = Strategy
  { dispatchSetup = setup
  , dispatchGameStep = \a b -> pure $ gameStep a b
  , makeMove = pure . move
  , prettyPrintState = Nothing
  }

spPath :: (Graph gr, Real b) => Node -> Node -> gr a b -> Maybe (b, LPath b)
spPath s t = (\x@(LP xs) -> if null xs then Nothing else Just (snd $ last xs, x)) .
             getLPath t .
             spTree s

computeDistances :: ActualMap -> IntSet -> Distances
computeDistances amap = let
  vertices = nodes amap
  in Distances .
     M.mapWithKey (\mine _ -> M.fromList $ map (\n -> (n, spPath mine n amap)) vertices) .
     M.fromSet id

onlyMines :: IntSet -> Distances -> Distances
onlyMines ms = Distances . M.map (M.filterWithKey (\node _ -> node `IS.member` ms)) . unDistances

distancesToList :: Distances -> [(Node,(Node,Maybe (Int, LPath Int)))]
distancesToList (Distances d) = concatMap (\(s,l) -> map (s,) $ M.toList l) $ M.toList d

computeFutures :: IntSet -> Distances -> Set Futures
computeFutures mines dist = let
  mineDist = onlyMines mines dist
  getDist = fst.fromJust.snd.snd
  (s,(t,Just (d,LP path))) = minimumBy (comparing getDist) $ filter ((/=0).getDist) $ distancesToList mineDist
  f1 = fst $ path!!1
  f2 = fst $ (reverse path)!!1
  mkFut x y = Futures (SiteId x) (SiteId y)
  in S.fromList $ if length path > 2
                  then [mkFut s f1, mkFut t f2]
                  else []

futureScore :: Distances -> Futures -> Int
futureScore (Distances m) (Futures (SiteId s) (SiteId t)) = let
  d = fromMaybe 0 $ fst <$> m M.! s M.! t
  in d*d*d

nodeScore :: Distances -> Int -> Int -> Int
nodeScore (Distances m) s n = let
  d = fromMaybe 0 $ fst <$> m M.! s M.! n
  in d*d

-- вычисляем счёт для заданной карты:
--  * в initial хранится отображение (шахта -> (место -> (расстояние, путь))) для начальной карты
--  * в now - то же для заданной карты, поэтому расстояние 0 - мы заклеймили точку
--  * мы вычисляем, какие фьючерсы выполнены (ф. выполнен, если от его шахты до его места 0)
--  * мы вычисляем, сумму квадратов расстояний от шахты до точки (для заклеймённых точек)
currentScore :: Distances -> Set Futures -> IntSet -> ActualMap -> Int
currentScore initial futures mines amap = let
  Distances now = computeDistances amap mines
  -- claimedVertices = filter ((==Just 0).snd.snd) $ distancesToList now
  (good,bad) = S.partition (\(Futures (SiteId s) (SiteId t)) -> (fst <$> now M.! s M.! t) == Just 0) futures
  scoreForMine m from s = M.foldrWithKey' (\n _ s' -> s' + nodeScore initial m n) s from
  in sum (S.map (futureScore initial) good) - sum (S.map (futureScore initial) bad) +
     M.foldrWithKey' scoreForMine 0 now

-- оцениваем путь:
--  * оценка пути - ожидаемый счёт после прохождения пути
--  * поскольку с каждым шагом вероятность пройти путь целиком падает,
--  * каждое следующее слагаемое (imaginaryScore) входит в сумму с множителем prob
-- Пусть N - количество понтёров (один из них - мы), M - количество оставшихся рёбер.
-- Пусть длина пути до текущего момента - C.
-- Вероятность захватить следующее ребро пути для одного случайного понтёра - 1/M
-- Для (N-1) независимого понтёра - вероятность захватить любое рёбро до данного момента -  С(N-1)/M.
computeScore :: ValuerState -> ((Route, LPath Int), Int) -> Double
computeScore VS { vsMap = amap, vsInitialDistances = initial, vsMines = mines, vsFutures = futures, vsPunters = punters } ((Route { distance = d, mine = SiteId s, site = SiteId t}, LP path), l) = let
  m = snd $ last path
  n = length $ filter (\(_,_,l) -> l>0) $ labEdges amap
  mapScore = currentScore initial futures mines amap
  multiplier = product [ prob punters m n i | i <- [0..m-1] ]
  prob p m n i = 1 - (fromIntegral $ p - 1) * (fromIntegral $ m - i) / (fromIntegral $ n - i*p)
  -- prob :: ActualMap -> Int -> Double
  -- prob m c = (fromIntegral $ punters - 1) * fromIntegral c / (fromIntegral $ length $ edges m)
  -- imaginaryScore m c = fromIntegral (currentScore initial futures mines m) * prob m c
  -- next (score,m) (s,t,c) = (score + imaginaryScore m c, setEdge0 (s,t) m)
  in -- fst $ foldl' next (0, amap) $ zipWith (\(s,x) (t,y) -> (s,t,y)) path $ tail path
    fromIntegral mapScore * multiplier

setup :: SetupReq -> (Maybe [Futures], ValuerState)
setup SetupReq { sqMap = gmap, sqPunters = totalPunters, sqPunter = me, sqSettings = settings }= let
  amap = createActualMap gmap
  mines = gmMines gmap
  mineSet = IS.fromList $ map unSiteId mines
  allDistances = computeDistances amap mineSet
  spans3 = toSpans allDistances
  -- mineDistances = onlyMines mineSet allDistances
  futures = if fromMaybe False $ fromMaybe (Just False) $ setFutures <$> settings
            then computeFutures mineSet allDistances
            else S.fromList []
  state = VS mineSet allDistances futures totalPunters me amap S.empty spans3 []
  Just path = mostPromissing state
  in (Just $ S.toList futures, state { vsCurPath = map fst $ unLPath $ snd path })

gameStep :: GameReq -> ValuerState -> ValuerState
gameStep GameReq { gqMove = moves } state@VS { vsMap = amap, vsSpans = spans, vsMe = me } = let
  amap' = removeMovedEdges me moves amap
  spans' = updateSpans me spans $ unMoves moves
  in state { vsMap = amap' }

continuePath :: ValuerState -> Path -> (MoveFn, ValuerState)
continuePath state@VS { vsMap = amap, vsOurEdges = ourEdges, vsSpans = spans } path = let
      (n1 : n2 : rest) = path
      edge = (n1,n2)
      antiEdge = (n2,n1)
      in if edge `S.member` ourEdges
         then move $ state { vsCurPath = n2 : rest }
         else (claim n1 n2, state { vsMap = setEdge0 edge amap
                                  , vsOurEdges = S.insert edge $ S.insert antiEdge ourEdges
                                  , vsSpans = updateSpansEdge True spans edge
                                  , vsCurPath = n2 : path})


move :: ValuerState -> (MoveFn, ValuerState)
move state@(VS { vsMap = amap, vsCurPath = path, vsMines = mines })
  | lenGT1 path && pathValid amap path = continuePath state path
  | otherwise = let
      state' = state -- { vsSpans = computeDistances amap mines }
      in case mostPromissing state' of
           Nothing -> (pass, state')
           Just (route, LP path)
             | lenGT1 path && pathValid amap (map fst path) -> continuePath state' $ map fst path
             | lenGT1 path -> move $ state' { vsSpans = Spans3 $ H.deleteMin $ unSpans3 $ vsSpans state' }
             | otherwise -> (pass, state')
  where
    lenGT1 (_ : _ : _) = True
    lenGT1 _ = False

updateSpans :: PunterId -> Spans3 -> [Move] -> Spans3
updateSpans me spans = foldl' updateSpans1 spans . filter ((/=me) . mPunter)

updateSpans1 :: Spans3 -> Move -> Spans3
updateSpans1 spans move = case move of
  Pass _ -> spans
  Claim _ (SiteId s) (SiteId t) -> updateSpansEdge False spans (s,t)
  Splurge _ moves -> foldl' (\spans (SiteId s, SiteId t) -> updateSpansEdge False spans (s,t)) spans $
                     zip moves (tail moves)
  Option _ (SiteId s) (SiteId t) -> spans -- we have already deleted this edge

updateSpansEdge :: Bool -> Spans3 -> Edge -> Spans3
updateSpansEdge myMove (Spans3 heap) edge
  | myMove = Spans3 $ H.map (\e@(Entry d (s,t,LP p)) -> if edge `elem` mkPath p
                                                        then Entry (d-1) (s,t,LP p)
                                                        else e) heap
  | otherwise = Spans3 $ H.filter (\(Entry d (s,t,LP p)) -> not $ edge `elem` mkPath p) heap
  where mkPath p = zipWith (\(s,_) (t,_) -> (s,t)) p (tail p)

toSpans :: Distances -> Spans3
toSpans now = Spans3 $ H.fromList $ concatMap swap $ distancesToList now
  where swap (s,(t,c)) = case c of
          Nothing -> []
          Just (d,p) -> [Entry (negate d) (s, t, p)]


mostPromissing :: ValuerState -> Maybe (Route, LPath Int)
mostPromissing state@(VS { vsSpans = spans, vsMap = amap }) = let
  lst = take 5 . filter (\(_, l) -> l /= 0) . map (\(Entry r (s,t,p)) -> let route = Route r (SiteId s) (SiteId t) in ((route,p), fromMaybe 0 $ spLen s t amap)) . toList . unSpans3 $ spans
  in if null lst
    then Nothing
    else Just . fst . maximumBy (compare `on` computeScore state) $ lst
  where
    splen (Route {mine = SiteId m, site = SiteId s}) = maybe 0 id $ spLen m s amap




-- algorithm3
--   :: ValuerState
--   -> Spans2
--   -> ActualMap -- ^ should only have unclaimed edges or those claimed by us (remove edges claimed by other players in outer function)
--   -> Set Edge  -- ^ set of rivers claimed by us
--   -> Path      -- ^ the path we currently follow (should be in sync with Spans)
--   -> (MoveFn, Spans2, ActualMap, Set Edge, Path)
-- algorithm3 state spans amap rivers cpath
--   | S.null spans  = trace "Empty spans" (pass, spans, amap, rivers, []) -- the heap is empty, I don't know what to do - pass
--
--   | S.null rivers = let -- it's our first move
--     Route {mine = SiteId src, site = SiteId dst} = S.findMax spans
--     path = sp src dst amap
--     in if src == dst || length path < 2
--       then trace "Empty rivers: recur with (S.deleteMax spans)" algorithm3 state (S.deleteMax spans) amap rivers [] -- try the next mine-site pair
--       else let (n1 : n2 : tl) = path
--                e = (n1, n2)
--            in (claim n1 n2, spans, setEdge0 e amap, S.insert e rivers, n2 : tl)
--
--   | length cpath < 2 = -- current path is over, find a new one
--     case mostPromissing state of
--       Nothing -> (pass, S.empty, amap, rivers, []) -- no promissing paths left, just always pass
--       Just (r@Route {mine = SiteId src, site = SiteId dst}) -> let
--         path = sp src dst amap
--         in trace ("Changing cpath: r = " ++ show r ++ " path = " ++ show path) $ if src == dst || length path < 2
--           then trace "Changing cpath: recur with (S.delete r spans)" algorithm3 state (S.delete r spans) amap rivers [] -- try the next mine-site pair
--           else let (n1 : n2 : tl) = path
--                    e = (n1, n2)
--                in if S.member e rivers
--                 then trace "Changing cpath: recur skipping edge" algorithm3 state (if null tl then S.delete r spans else spans) amap rivers (n2 : tl) -- skip already claimed edge
--                 else (claim n1 n2, spans, setEdge0 e amap, S.insert e rivers, n2 : tl)
--
--   | pathValid amap cpath = let
--     (n1 : n2 : tl) = cpath
--     e = (n1, n2)
--     in if S.member e rivers
--       then trace "Valid cpath: recur skipping edge" algorithm3 state spans amap rivers (n2 : tl) -- skip already claimed edge
--       else (claim n1 n2, spans, setEdge0 e amap, S.insert e rivers, n2 : tl)
--
--   | otherwise = -- current path isn't there anymore, try another one for the same mine-site
--     case mostPromissing state of
--       Nothing -> (pass, S.empty, amap, rivers, []) -- no promissing paths left, just always pass
--       Just (r@Route {mine = SiteId src, site = SiteId dst}) -> let
--         path = sp src dst amap
--         in trace ("Last close: r = " ++ show r ++ " path = " ++ show path) $ if src == dst || length path < 2
--           then trace "Last close: recur with (S.delete r spans)" algorithm3 state (S.delete r spans) amap rivers [] -- try the next mine-site pair
--           else let (n1 : n2 : tl) = path
--                    e = (n1, n2)
--                in if S.member e rivers
--                 then trace "Last close: recur skipping edge" algorithm3 state spans amap rivers (n2 : tl) -- skip already claimed edge
--                 else (claim n1 n2, spans, setEdge0 e amap, S.insert e rivers, n2 : tl)
