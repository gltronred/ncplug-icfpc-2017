module Types.Strategy where

import Types.Protocol

import Data.Text (Text)

data Strategy state = Strategy
  { dispatchSetup :: SetupReq -> (Maybe [Futures], state)
  , dispatchGameStep :: GameReq -> state -> IO state
  , makeMove :: state -> IO (PunterId -> Move, state)
  , prettyPrintState :: Maybe (state -> Text)
  }
