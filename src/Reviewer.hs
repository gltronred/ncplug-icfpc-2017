{-# LANGUAGE OverloadedStrings #-}

module Reviewer where

import           Types
import           Strategies.Common

import           Control.Monad
import qualified Data.ByteString.Lazy.Char8 as BS
import           Data.Maybe (fromMaybe)
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.IO as T
import           System.Directory
import           System.Environment
import           System.Exit
import           System.IO
import           System.Process
import           Text.Printf

reviewerMain :: IO ()
reviewerMain = do
  args <- getArgs
  case args of
    [dir] -> work dir
    _ -> do
      T.hPutStrLn stderr "Usage: ./reviewer <dir>"
      T.hPutStrLn stderr "It accepts logs through stdin"
      exitFailure

data GraphInfo = GraphInfo { giMap :: ActualMap
                           , giFutures :: Maybe [Futures]
                           , giMines :: [Int]
                           , giMe :: PunterId
                           } deriving (Eq,Show,Read)

work :: String -> IO ()
work dir = do
  setCurrentDirectory dir
  input <- T.getContents
  let ls = drop 2 $
           filter (\line -> "<- "`T.isPrefixOf`line || "-> "`T.isPrefixOf`line) $
           T.lines input
      pairs = zip [1::Int ..] $ by2 ls
  void $ loop pairs (GraphInfo empty Nothing [] (PunterId $ -1)) $ \(n,(reqLine, respLine)) gi -> do
    -- T.putStrLn $ T.concat [ "Req: ", reqLine, "\n"
    --                       , "Res: ", respLine]
    let req = decodeProtoRequest $ BS.fromStrict $ T.encodeUtf8 $ T.drop 3 reqLine
        resp= decodeProtoRequest $ BS.fromStrict $ T.encodeUtf8 $ T.drop 3 respLine
        gi' = case (req,resp) of
                (SetupRq sq, SetupRes sp) -> initialize sq sp
                (GameRq gq, GameRes gp) -> update gq gp gi
    let name = "step-" ++ printf "%04d" n ++ ".dot"
    T.writeFile name $ render gi
    spawnProcess "sfdp" ["-O", "-Tpng", name]
    return gi'
   where
     loop xs s act = case xs of
       [] -> return s
       (x:xs) -> act x s >>= (\s' -> loop xs s' act)


by2 :: [a] -> [(a,a)]
by2 [] = []
by2 [x]= []
by2 (x:y:zs) = (x,y) : by2 zs

initialize :: SetupReq -> SetupResp -> GraphInfo
initialize sq sp = let
  graph = createActualMap (sqMap sq)
  mines = map unSiteId $ gmMines $ sqMap sq
  futs = spFutures sp
  me = sqPunter sq
  in GraphInfo graph futs mines me

update :: GameReq -> GameResp -> GraphInfo -> GraphInfo
update gq gp gi = let
  me = giMe gi
  graph = giMap gi
  graph' = foldr (\m g -> if mPunter m==me then g else updateMove 2 m g) graph $ unMoves $ gqMove gq
  graph''= updateMove 3 (gpMove gp) graph'
  in gi { giMap = graph'' }

updateMove :: Int -> Move -> ActualMap -> ActualMap
updateMove color move graph = case move of
  Pass _ -> graph
  Claim _ (SiteId s) (SiteId t) ->
    insEdge (t,s,color) $
    insEdge (s,t,color) $
    delEdge (s,t) $
    delEdge (t,s) graph

render :: GraphInfo -> Text
render GraphInfo { giMap = g, giFutures = mfs, giMines = ms } = let
  fs = map (unSiteId . futTarget) $ fromMaybe [] mfs
  in T.intercalate "\n" $
     "graph actual {" :
     map (\v -> T.concat ["  v", T.pack $ show v
                         , " [ label=\"", T.pack $ show v, "\""
                         , " "
                         , if v `elem` fs then "shape=diamond" else "shape=circle"
                         , " "
                         , if v `elem` ms then "fillcolor=yellow style=filled" else ""
                         , "];"
                         ]) (nodes g) ++
     [""] ++
     map (\(s,t,c) -> T.concat ["  v", T.pack $ show s
                               , " -- "
                               , "v", T.pack $ show t
                               , " [color="
                               , case c of
                                   1 -> "black"
                                   2 -> "blue"
                                   3 -> "red"
                               , "];"
                               ]) (labEdges g) ++
     ["}"]

