{-# LANGUAGE OverloadedStrings #-}

module Strategies.Dummy where

import Types

dummyStrategy :: Strategy ()
dummyStrategy = Strategy
  { dispatchSetup = \_ -> (Nothing, ())
  , dispatchGameStep = \_ s -> pure s
  , makeMove = \() -> pure (pass, ())
  , prettyPrintState = Just $ \() -> ""
  }

