{-# LANGUAGE TupleSections #-}

module Strategies.Common where

import           Control.Concurrent
import           Control.Concurrent.Async
import           Data.Graph.Inductive.Graph
import qualified Data.Graph.Inductive.Internal.Heap as H
import           Data.Set (Set)
import qualified Data.Set as S
import           System.IO

import           Types

createActualMap :: GameMap -> ActualMap
createActualMap (GameMap { gmSites = ss, gmRivers = rs }) = let
  nodeGraph = foldr (\(Site (SiteId i)) -> insNode (i, ())) empty ss
  edgeGraph = foldr (\(River (SiteId s) (SiteId t)) -> insEdge (s, t, 1) . insEdge (t, s, 1)) nodeGraph rs
  in edgeGraph

createSpans :: ActualMap -> [SiteId] -> Spans
createSpans edgeGraph mines = H.build $ [(negate dist, (SiteId mine, SiteId site)) | SiteId mine <- mines, LP path <- spTree mine edgeGraph, let (site, dist) = head path, mine /= site]

createSpans2 :: ActualMap -> [SiteId] -> Spans2
createSpans2 edgeGraph mines = S.fromList $ [Route dist (SiteId mine) (SiteId site) | SiteId mine <- mines, LP path <- spTree mine edgeGraph, let (site, dist) = head path, mine /= site]

removeMovedEdge :: PunterId -> Move -> ActualMap -> ActualMap
removeMovedEdge me move | mPunter move == me = id
                        | otherwise = case move of
                            Pass _ -> id
                            Claim _ (SiteId s) (SiteId t) -> delEdge (s,t) . delEdge (t,s)
                            Splurge _ sites -> let
                              ns = map unSiteId sites
                              path = zip ns (tail ns)
                              in foldr (\(s,t) f -> delEdge (s,t) . delEdge (t,s) . f) id path
                            Option _ _ _ -> id

removeMovedEdges :: PunterId -> Moves -> ActualMap -> ActualMap
removeMovedEdges me (Moves moves) graph = foldr (removeMovedEdge me) graph moves

claimAndMark :: Int -> Int -> Set Int -> (MoveFn, Set Int)
claimAndMark s t claimed = (claim s t, S.insert s $ S.insert t claimed)

-- cycleFrom :: [Strategy state] -> Strategy (Int, [state])
-- cycleFrom strategies = Strategy
--   { dispatchSetup = \setup -> let
--       (ffs, ss) = unzip $ map (flip dispatchSetup setup) strategies
--       in (mconcat ffs, (0,ss))
--   , dispatchGameStep = \step (idx, states) -> let
--       states' = map (\(strategy,state) -> dispatchGameStep strategy step state) $
--                 zip strategies states
--       in (idx,states')
--   , makeMove = \(idx, states) -> let
--       (move, state') = makeMove (strategies !! idx) (states !! idx)
--       idx' = (idx+1) `mod` length strategies
--       in (move, (idx', take idx states ++ [state'] ++ drop (idx+1) states))
--   , prettyPrintState = Nothing
--   }

select :: Strategy s1 -> Strategy s2 -> (Gr () Int -> Bool) -> Strategy (Either s1 s2)
select x y b = Strategy
  { dispatchSetup = \setup -> let
      g = createActualMap $ sqMap setup
      in if not $ b g
         then Left <$> dispatchSetup x setup
         else Right <$> dispatchSetup y setup
  , dispatchGameStep = \step estate -> case estate of
      Left s1 -> Left <$> dispatchGameStep x step s1
      Right s2 -> Right <$> dispatchGameStep y step s2
  , makeMove = \estate -> case estate of
      Left s1 -> fmap Left <$> makeMove x s1
      Right s2 -> fmap Right <$> makeMove y s2
  , prettyPrintState = Nothing
  }

withoutTimeouts :: (s -> ActualMap) -> Strategy s -> Strategy (Maybe Edge, s)
withoutTimeouts getMap s =
  Strategy
  { dispatchSetup = \i -> let (f,t) = dispatchSetup s i in (f,(Nothing,t))
  , dispatchGameStep = gs
  , makeMove = move
  , prettyPrintState = Nothing
  }
  where
    gs step (_,state) = do
      r <- race (timeout state) (dispatchGameStep s step state)
      return $ case r of
        Left e -> (Just e, state)
        Right state' -> (Nothing, state')
    move (timedOut, state) = case timedOut of
      Just e -> return (uncurry claim e, (Nothing,state))
      Nothing -> do
        r <- race (timeout state) (makeMove s state)
        return $ case r of
          Left e -> (uncurry claim e, (Nothing,state))
          Right (move, state') -> (move, (Nothing,state'))
    timeout state = do
      let (s,t,_) = head $! labEdges $ getMap state
          edge = (s,t)
      threadDelay 850000
      hPutStrLn stderr "FAIL"
      return edge

