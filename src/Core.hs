{-# LANGUAGE BangPatterns #-}

module Core where

import           Data.Graph.Inductive.Internal.Heap as H (deleteMin, findMin, isEmpty)
import           Data.Function (on)
import           Data.IntSet (IntSet)
import qualified Data.IntSet as IS
import           Data.List (maximumBy)
import           Data.Set (Set)
import qualified Data.Set as S

import Debug.Trace

import Types.Core
import Types.Protocol


mostPromissing :: Spans2 -> ActualMap -> Maybe Route
mostPromissing spans amap = let
  lst = take 10 . filter (\(_, l) -> l /= 0) . map (\r -> (r, splen r)) . S.toDescList $ spans
  in if null lst
    then Nothing
    else Just . fst . maximumBy (compare `on` score) $ lst
  where
    splen (Route {mine = SiteId m, site = SiteId s}) = maybe 0 id $ spLen m s amap
    score :: (Route, Int) -> Double
    score (Route {distance = d}, len) = fromIntegral (d*d) / fromIntegral len

addRiver :: Edge -> Set Edge -> Set Edge
addRiver e@(s, t) = S.insert (t, s) . S.insert e


algorithm3
  :: Spans2
  -> ActualMap -- ^ should only have unclaimed edges or those claimed by us (remove edges claimed by other players in outer function)
  -> Set Edge  -- ^ set of rivers claimed by us
  -> Path      -- ^ the path we currently follow (should be in sync with Spans)
  -> (MoveFn, Spans2, ActualMap, Set Edge, Path)
algorithm3 spans amap rivers cpath
  | S.null spans  = let -- the heap is empty, claim any remaining rivers
    onMap = S.fromList $ edges amap
    diff  = S.difference onMap rivers
    e@(n1, n2) = S.elemAt 0 diff
    in if S.null diff
      then (pass, spans, amap, rivers, []) -- actually game should have ended by now
      else (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, [])

  | S.null rivers = let -- it's our first move
    Route {mine = SiteId src, site = SiteId dst} = S.findMax spans
    path = sp src dst amap
    in if src == dst || length path < 2
      then algorithm3 (S.deleteMax spans) amap rivers [] -- try the next mine-site pair
      else let (n1 : n2 : tl) = path
               e = (n1, n2)
           in (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, n2 : tl)

  | length cpath < 2 = -- current path is over, find a new one
    case mostPromissing spans amap of
      Nothing -> (pass, S.empty, amap, rivers, []) -- no promissing paths left, just always pass
      Just (r@Route {mine = SiteId src, site = SiteId dst}) -> let
        path = sp src dst amap
        in if src == dst || length path < 2
          then algorithm3 (S.delete r spans) amap rivers [] -- try the next mine-site pair
          else let (n1 : n2 : tl) = path
                   e = (n1, n2)
               in if S.member e rivers
                then algorithm3 (if null tl then S.delete r spans else spans) amap rivers (n2 : tl) -- skip already claimed edge
                else (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, n2 : tl)

  | pathValid amap cpath = let
    (n1 : n2 : tl) = cpath
    e = (n1, n2)
    in if S.member e rivers
      then algorithm3 spans amap rivers (n2 : tl) -- skip already claimed edge
      else (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, n2 : tl)

  | otherwise = -- current path isn't there anymore, try another one for the same mine-site
    case mostPromissing spans amap of
      Nothing -> (pass, S.empty, amap, rivers, []) -- no promissing paths left, just always pass
      Just (r@Route {mine = SiteId src, site = SiteId dst}) -> let
        path = sp src dst amap
        in if src == dst || length path < 2
          then algorithm3 (S.delete r spans) amap rivers [] -- try the next mine-site pair
          else let (n1 : n2 : tl) = path
                   e = (n1, n2)
               in if S.member e rivers
                then algorithm3 spans amap rivers (n2 : tl) -- skip already claimed edge
                else (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, n2 : tl)

init2 :: Spans -> ActualMap -> Path
init2 spans amap = let
  (_, (SiteId src, SiteId dst)) = findMin spans
  path = sp src dst amap
  in if src == dst || length path < 2
     then init2 (deleteMin spans) amap -- try the next mine-site pair
     else path


algorithm2
  :: Spans
  -> ActualMap -- ^ should only have unclaimed edges or those claimed by us (remove edges claimed by other players in outer function)
  -> Set Edge  -- ^ set of rivers claimed by us
  -> Path      -- ^ the path we currently follow (should be in sync with Spans)
  -> (MoveFn, Spans, ActualMap, Set Edge, Path)
algorithm2 !spans !amap !rivers !cpath
  | H.isEmpty spans  = let -- the heap is empty, claim any remaining rivers
    onMap = edges $ elfilter (> 0) amap
    in if null onMap
      then (pass, spans, amap, rivers, []) -- actually game should have ended by now
      else let e@(n1, n2) = head onMap
        in (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, [])

  | S.null rivers   = let -- it's our first move
      (n1 : n2 : tl) = cpath
      e = (n1, n2)
      in (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, n2 : tl)

  | length cpath < 2 = let -- current path is over, find a new one
    spans' = deleteMin spans
    in if H.isEmpty spans'
      then algorithm2 spans' amap rivers []
      else let
        (_, (SiteId src, SiteId dst)) = findMin spans'
        path = sp src dst amap
        in if src == dst || pathTooShort path
          then algorithm2 spans' amap rivers [] -- try the next mine-site pair
          else let (n1 : n2 : tl) = path
                   e = (n1, n2)
               in if S.member e rivers
                then algorithm2 spans' amap rivers (n2 : tl) -- skip already claimed edge
                else (claim n1 n2, spans', setEdge0 e amap, addRiver e rivers, n2 : tl)

  | pathValid amap cpath = let
    (n1 : n2 : tl) = cpath
    e = (n1, n2)
    in if S.member e rivers
      then algorithm2 spans amap rivers (n2 : tl) -- skip already claimed edge
      else (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, n2 : tl)

  | otherwise = let -- current path isn't there anymore, try another one for the same mine-site
    (_, (SiteId src, SiteId dst)) = findMin spans
    path = sp src dst amap
    in if src == dst || pathTooShort path
      then algorithm2 spans amap rivers [] -- try the next mine-site pair
      else let (n1 : n2 : tl) = path
               e = (n1, n2)
           in if S.member e rivers
            then algorithm2 spans amap rivers (n2 : tl) -- skip already claimed edge
            else (claim n1 n2, spans, setEdge0 e amap, addRiver e rivers, n2 : tl)
  where
    pathTooShort []  = True
    pathTooShort [_] = True
    pathTooShort _   = False


-- | Main algorithm
algorithm
  :: Spans
  -> ActualMap -- ^ should only have unclaimed edges or those claimed by us (remove edges claimed by other players in outer function)
  -> IntSet    -- ^ set of site ids claimed by us
  -> (MoveFn, Spans, IntSet)
algorithm spans amap frontier
  | H.isEmpty spans = (pass, spans, frontier) -- the heap is empty, I don't know what to do - pass
  | IS.null frontier = let
--   It's our first move we haven't claimed any rivers (hence any sites) yet.
    (_, (SiteId src, SiteId dst)) = findMin spans
    path = sp src dst amap
    in if src == dst
      then algorithm (deleteMin spans) amap frontier -- ignore cycles
      else case path of
        (n1 : n2 : _) -> (claim n1 n2, spans, IS.insert n1 $ IS.insert n2 frontier)
        _ -> (pass, spans, frontier) -- empty path, I don't know what to do - pass
  | otherwise = let
--   In the outer loop we choose one of farthest mine-site pairs.
    (_, (SiteId src, SiteId dst)) = findMin spans
  --   Then we try to build a shortest path between them w.r.t.
  --   already claimed and still available rivers.
    gvOut = gvdOut (IS.toList frontier) amap
    mpath = nearestPath dst gvOut
    in if src == dst
      then algorithm (deleteMin spans) amap frontier -- ignore cycles
      else case mpath of
  --   If no path exists, ignore this pair and go to outer loop.
      Nothing -> algorithm (deleteMin spans) amap frontier
      Just path -> if length path > 1
        then let (n1 : n2 : _) = reverse path
          in (claim n1 n2, spans, IS.insert n1 $ IS.insert n2 frontier) -- path must have at least 2 nodes (1 edge) I think
                                                                 -- n1 should belong to frontier as we build gvOut from them
        else algorithm (deleteMin spans) amap frontier -- fuck it and try the next one
