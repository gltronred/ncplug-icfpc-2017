{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE StandaloneDeriving #-}

module Strategies.Achiever where

import           Data.Aeson
import           Data.Binary (Binary)
import qualified Data.Graph.Inductive.Internal.Heap as H
import           Data.IntSet (IntSet)
import qualified Data.IntSet as IS
import           Data.Set (Set)
import qualified Data.Set as S
import           GHC.Generics

import           Types
import           Core
import           Strategies.Common


type GameState  = (Spans, ActualMap, IntSet, PunterId)
type GameState2 = (Spans, ActualMap, Set Edge, Path, PunterId)
type GameState3 = (Spans2, ActualMap, Set Edge, Path, PunterId)

achieverStrategy :: Strategy GameState
achieverStrategy = Strategy
  { dispatchGameStep = \a b -> pure $ gameStep a b
  , dispatchSetup = setup
  , makeMove = pure . move
  , prettyPrintState = Nothing
  }

setup :: SetupReq -> (Maybe [Futures], GameState)
setup (SetupReq { sqMap = gm, sqPunter = me }) = let
  amap  = createActualMap gm
  spans = createSpans amap (gmMines gm)
  in (Nothing, (spans, amap, IS.empty, me))

gameStep :: GameReq -> GameState -> GameState
gameStep (GameReq { gqMove = ms }) (spans, amap, frontier, me) = let
  amap' = removeMovedEdges me ms amap
  in (spans, amap', frontier, me)

move :: GameState -> (PunterId -> Move, GameState)
move (spans, amap, frontier, me) = let
  (movefn, spans', frontier') = algorithm spans amap frontier
  in (movefn, (spans', amap, frontier', me))

achiever2Strategy :: Strategy GameState2
achiever2Strategy = Strategy
  { dispatchGameStep = \a b -> pure $ gameStep2 a b
  , dispatchSetup = setup2
  , makeMove = pure . move2
  , prettyPrintState = Nothing
  }

setup2 :: SetupReq -> (Maybe [Futures], GameState2)
setup2 (SetupReq { sqMap = gm, sqPunter = me }) = let
  amap  = createActualMap gm
  spans = createSpans amap (gmMines gm)
  cpath = init2 spans amap
  in (Nothing, (spans, amap, S.empty, cpath, me))

gameStep2 :: GameReq -> GameState2 -> GameState2
gameStep2 (GameReq { gqMove = ms }) (spans, amap, rivers, cpath, me) = let
  amap' = removeMovedEdges me ms amap
  in (spans, amap', rivers, cpath, me)

move2 :: GameState2 -> (PunterId -> Move, GameState2)
move2 (spans, amap, rivers, cpath, me) = let
  (movefn, spans', amap', rivers', cpath') = algorithm2 spans amap rivers cpath
  in (movefn, (spans', amap', rivers', cpath', me))

achiever3Strategy :: Strategy GameState3
achiever3Strategy = Strategy
  { dispatchGameStep = \a b -> pure $ gameStep3 a b
  , dispatchSetup = setup3
  , makeMove = pure . move3
  , prettyPrintState = Nothing
  }

setup3 :: SetupReq -> (Maybe [Futures], GameState3)
setup3 (SetupReq { sqMap = gm, sqPunter = me }) = let
  amap  = createActualMap gm
  spans = createSpans2 amap (gmMines gm)
  in (Nothing, (spans, amap, S.empty, [], me))

gameStep3 :: GameReq -> GameState3 -> GameState3
gameStep3 (GameReq { gqMove = ms }) (spans, amap, rivers, cpath, me) = let
  amap' = removeMovedEdges me ms amap
  in (spans, amap', rivers, cpath, me)

move3 :: GameState3 -> (PunterId -> Move, GameState3)
move3 (spans, amap, rivers, cpath, me) = let
  (movefn, spans', amap', rivers', cpath') = algorithm3 spans amap rivers cpath
  in (movefn, (spans', amap', rivers', cpath', me))


deriving instance Generic (Heap a b)

instance (FromJSON a, FromJSON b) => FromJSON (Heap a b)
instance (ToJSON a, ToJSON b) => ToJSON (Heap a b)
instance (Binary a, Binary b) => Binary (Heap a b)

