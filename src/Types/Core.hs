{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Types.Core
  ( module X
  , module Types.Core
  ) where

import Data.Aeson
import Data.Aeson.Types
import Data.Binary
import Data.Function (on)
import Data.Graph.Inductive as X
import Data.Graph.Inductive.PatriciaTree as G
import Data.Maybe (listToMaybe)
import Data.Set (Set)
import GHC.Generics

import Types.Protocol


type Spans = Heap Int (SiteId, SiteId) -- ^ Min heap of spans betwean mines and furthest sites.
                                       --   Distances are inverted as we need max heap.

data Route = Route
  { distance :: Int
  , mine     :: SiteId
  , site     :: SiteId
  } deriving (Eq, Show, Read, Generic)

instance Ord Route where
  (<=) = (<=) `on` distance
  (<)  = (<)  `on` distance
  (>=) = (>=) `on` distance
  (>)  = (>)  `on` distance
  compare = compare `on` distance

instance FromJSON Route where
  parseJSON = genericParseJSON defaultOptions
instance ToJSON Route where
  toJSON = genericToJSON defaultOptions
  toEncoding = genericToEncoding defaultOptions

type Spans2 = Set Route

type ActualMap = Gr () Int -- ^ Nodes are unlabeled, edges are labeled with length

instance FromJSON a => FromJSON (LPath a) where
  parseJSON v = LP <$> parseJSON v
instance ToJSON a => ToJSON (LPath a) where
  toJSON (LP a) = toJSON a
  toEncoding (LP a) = toEncoding a

instance (FromJSON a, FromJSON b) => FromJSON (Gr a b) where
  parseJSON (Object v) = buildGr <$> v .: "contexts"
  parseJSON invalid = typeMismatch "Gr a b" invalid
instance (ToJSON a, ToJSON b) => ToJSON (Gr a b) where
  toJSON graph = object [ "contexts" .= ufold (:) [] graph ]
  toEncoding graph = pairs ("contexts" .= ufold (:) [] graph )
instance (Binary a, Binary b) => Binary (Gr a b) where
  get = buildGr <$> get
  put = put . ufold (:) []

type MoveFn = PunterId -> Move

claim :: Int -> Int -> MoveFn
claim source target = \ident -> Claim ident (SiteId source) (SiteId target)

pass :: MoveFn
pass = Pass

splurge :: [Int] -> MoveFn
splurge route = \ident -> Splurge ident (map SiteId route)

option :: Int -> Int -> MoveFn
option s t = \ident -> Option ident (SiteId s) (SiteId t)


setEdge0 :: (DynGraph gr, Num b) => Edge -> gr a b -> gr a b
setEdge0 e@(s, t) = insEdges [(s, t, 0), (t, s, 0)] . delEdge (t, s) . delEdge e

pathValid :: Graph gr => gr a b -> Path -> Bool
pathValid g []  = False -- empty path is stupid
pathValid g [_] = False -- one-node path is also stupid
pathValid g [s, t] = hasEdge g (s, t) -- ground case
pathValid g (s : t : tl) = hasEdge g (s, t) && pathValid g (t : tl)

-- | Length of the shortest path between two nodes.
spLen :: (Graph gr, Real b)
      => Node -- ^ Start
      -> Node -- ^ Destination
      -> gr a b
      -> Maybe b
spLen s t = listToMaybe . map snd . filter ((==t).fst) . map (head . unLPath) . spTree s
