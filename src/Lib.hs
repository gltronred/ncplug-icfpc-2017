{-# LANGUAGE BangPatterns #-}
{-# LANGUAGE OverloadedStrings #-}

module Lib
  ( module Lib
  , reviewerMain
  ) where

import           Types

import           Strategies.Achiever (achieverStrategy, achiever2Strategy, achiever3Strategy)
import           Strategies.Common (select, withoutTimeouts)
import           Strategies.Valuer (valuerStrategy, vsMap)

import           Reviewer (reviewerMain)

import           Conduit
import           Control.Monad
import           Data.Aeson
import           Data.Binary (Binary)
import           Data.Bool
import qualified Data.ByteString.Char8 as B
import           Data.ByteString.Lazy.Builder
import           Data.ByteString.Lazy.Char8 (ByteString)
import qualified Data.ByteString.Lazy.Char8 as BS
import           Data.Char
import           Data.Conduit.ByteString.Builder
import           Data.Conduit.Network
import           Data.Graph.Inductive.Graph
import           Data.Maybe (fromMaybe)
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.IO as T
import           Debug.Trace
import           System.Environment
import           System.IO

theName :: Text
theName = "NCPLUG"

-- theStrategy :: Strategy ()
theStrategy = withoutTimeouts getMap $ select achiever2Strategy valuerStrategy $ \g -> let
  vertices = noNodes g
  edges = size g `div` 2 -- undirected graph
  in if trace "vertices" vertices < 50
     then False
     else if trace "VERTICES" vertices > 100
          then True
          else trace "density" fromIntegral edges / fromIntegral vertices < 1.5
  where getMap (Left (_, a2, _, _, _)) = a2
        getMap (Right vs) = vsMap vs

runStrategyConduit :: MonadIO m
                   => Strategy state
                   -> ConduitM Request (Maybe state,Response) m ()
runStrategyConduit strategy = do
  yield (Nothing, HandshakeRes $ HandshakeResp theName)
  HandshakeRq (HandshakeReq name) <- throwNothing <$> await
  SetupRq setup <- throwNothing <$> await
  let hasFutures = fromMaybe False $ maybe (Just False) setFutures $ sqSettings setup
      (mfutures, state) = dispatchSetup strategy setup
      me = sqPunter setup
      futures = if hasFutures then mfutures else Nothing
  yield (Just state, SetupRes $ SetupResp me futures)
  loop me state
  where
    throwNothing = maybe (error "Upstream closed. Failing!") id
    printScore (PunterId me) (Score (PunterId pid) score) =
      T.putStrLn $ T.concat [ bool " " ">" $ me == pid
                            , T.pack $ show pid
                            , bool " " "<" $ me == pid
                            , " "
                            , T.pack $ show score
                            ]
    loop me state = do
      req <- throwNothing <$> await
      case req of
        GameRq gq -> do
          state' <- liftIO $ dispatchGameStep strategy gq state
          (move, state'') <- liftIO $ makeMove strategy state'
          yield (Just state'', GameRes $ GameResp $ move me) >> (loop me $! state'')
        StopRq sq -> do
          mapM_ (liftIO . printScore me) $ ssScores $ stopStop sq
        r -> error $ "Something went wrong: not waited: " ++ show r

runStrategyConduitE :: (MonadIO m, Show state)
                    => Strategy state
                    -> ConduitM (OfflineReq (state, PunterId)) (OfflineResp (state, PunterId)) m ()
runStrategyConduitE strategy = do
  yield $ OfflineResp (HandshakeRes $ HandshakeResp theName) Nothing
  OfflineReq (HandshakeRq (HandshakeReq name)) Nothing <- throwNothing <$> await
  OfflineReq req mstate <- throwNothing <$> await
  case (req, mstate) of
    (SetupRq setup, Nothing) -> let
      hasFutures = fromMaybe False $ maybe (Just False) setFutures $ sqSettings setup
      (mfutures, state) = dispatchSetup strategy setup
      me = sqPunter setup
      futures = if hasFutures then mfutures else Nothing
      in yield $ OfflineResp (SetupRes $ SetupResp me futures) $ Just (state, me)
    (GameRq step, Just (state, me)) -> do
      state' <- liftIO $ dispatchGameStep strategy step state
      (move, state'') <- liftIO $ makeMove strategy state'
      yield $ OfflineResp (GameRes $ GameResp $ move me) $ Just (state'', me)
    (StopRq stop, Just (state, me)) -> return ()
    (r, s) -> error $ "Something went wrong: request " ++ show r ++ "; state " ++ show s
  where
    throwNothing = fromMaybe (error "Upstream closed! Failing!")

debugConduit :: MonadIO m => (x -> Text) -> ConduitM x x m ()
debugConduit f = mapMC (\x -> liftIO (T.hPutStrLn stderr $ f x) >> return x)

collectChunk :: Monad m => ConduitM B.ByteString B.ByteString m ()
collectChunk = loop
  where loop = do
          beforeColon <- takeWhileCE (/=fromIntegral (ord ':')) =$= sinkList
          unless (null beforeColon) $ do
            let n = read $ B.unpack $ B.concat beforeColon
            afterColon <- takeCE (n+1) =$= sinkList
            unless (null afterColon) $ do
              yield $ B.concat $ beforeColon ++ afterColon
              loop

idC = awaitForever yield

runOnline :: (Show state, FromJSON state, ToJSON state)
          => Strategy state
          -> ByteString
          -> Int
          -> Bool
          -> IO ()
runOnline strategy host port showState = do
  let settings = clientSettings port $ BS.toStrict host
  runTCPClient settings $ \ad -> do
    runConduitRes
      $  appSource ad
      .| collectChunk
      .| debugConduit (\req -> T.concat ["<- ", T.decodeUtf8 req])
      .| mapC (decodeProtoRequest . BS.fromStrict)
      .| runStrategyConduit strategy
      .| (if showState then debugConduit (\(mst,_) -> T.concat ["== ", maybe "NA" (fromMaybe (T.pack . show) $ prettyPrintState strategy) mst]) else idC)
      .| mapC (BS.toStrict . encodeProtoResponse . snd)
      .| debugConduit (\resp -> T.concat ["-> ", T.decodeUtf8 resp])
      .| appSink ad

runOffline :: (FromJSON state, ToJSON state, Show state)
           => Strategy state
           -> IO ()
runOffline strategy = runConduitRes
  $  sourceHandle stdin
  .| collectChunk
  -- .| debugConduit (\r -> T.concat ["<- ", T.decodeUtf8 r])
  .| mapC (decodeProtoRequest . BS.fromStrict)
  .| runStrategyConduitE strategy
  .| mapC (BS.toStrict . encodeProtoResponse)
  -- .| debugConduit (\r -> T.concat ["-> ", T.decodeUtf8 r])
  .| sinkHandle stdout

punterMain :: IO ()
punterMain = do
  args <- getArgs
  case args of
    [] -> runOffline theStrategy
    [hostStr, portStr] -> do
      T.hPutStrLn stderr $ T.concat [ "Connecting to ", T.pack hostStr, ":", T.pack $ portStr ]
      let host = BS.pack hostStr
          port = read portStr
      runOnline theStrategy host port True
    [hostStr, portStr, "--no-state"] -> do
      T.hPutStrLn stderr $ T.concat [ "Connecting to ", T.pack hostStr, ":", T.pack $ portStr ]
      let host = BS.pack hostStr
          port = read portStr
      runOnline theStrategy host port False
    _ -> T.hPutStrLn stderr $ T.concat [ "Usage: ./punter", "\n"
                                       , "       ./punter <host> <port> [--no-state]"]
