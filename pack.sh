#!/bin/bash

PKG_ROOT=/tmp/ncplug-icfpc-pack
NAME=icfp-b6b49220-d521-45ba-b3c6-34683bac7901.tar.gz 

if [ $# == 0 ]; then

rm -rf ${PKG_ROOT}
git clone git@bitbucket.org:gltronred/ncplug-icfpc-2017.git ${PKG_ROOT}/src

pushd ${PKG_ROOT}/src
stack install --local-bin-path=.. --system-ghc
cp README.md ../README
cp PACKAGES ../PACKAGES
cp install ../install
popd

pushd ${PKG_ROOT}
tar czf ${NAME} README PACKAGES install punter src/
popd

cp ${PKG_ROOT}/${NAME} ./
md5sum ${NAME}

drive push -force -fix-clashes -no-prompt ${NAME}
drive share icfp-b6b49220-d521-45ba-b3c6-34683bac7901.tar.gz -emails 'icfpcontest2017@gmail.com' -role reader

fi

url=$(drive url ${NAME} | sed 's/^.*: //g;')

md5=$(md5sum ${NAME} | sed 's/ .*$//g;')
token=$(curl -s -H "Connection: keep-alive" http://punter.inf.ed.ac.uk:9000/update/?token=b6b49220-d521-45ba-b3c6-34683bac7901 | grep '"_k"' | sed -n '2 {s/^.* value="\([^"]*\)".*$/\1/g; p;}')
# token=$(cat token.txt)
cat="input_8"

cat <<EOF
token ${token}
url ${url}
md5 ${md5}
EOF

curl -X POST -L -s -H 'Expect: ' http://punter.inf.ed.ac.uk:9000/update/?token=b6b49220-d521-45ba-b3c6-34683bac7901 -H "Referer: http://punter.inf.ed.ac.uk:9000/update/?token=b6b49220-d521-45ba-b3c6-34683bac7901" -H "Host: punter.inf.ed.ac.uk:9000" -H "Connection: keep-alive" -H "Upgrade-Insecure-Requests: 1" --data-urlencode "_k=${token}" --data-urlencode "input_4=${url}" --data-urlencode "input_5=${md5}" --data-urlencode "input_6=here" | grep lightgreen | sed 's/<[^>]*>//g; /_/d; s/  */ /g;'


